﻿using UnityEngine.InputSystem;
using System;
using Unity.VisualScripting;

namespace Digitom.VSInputSystem.RunTime
{
    public class InputActionRebind : InputSystemActionFlowBase
    {
        [DoNotSerialize] public ValueInput disableActionBeforeRebind;
        [DoNotSerialize] public ValueInput enableActionOnRebind;
        [DoNotSerialize] public ControlOutput onRebind;
        [DoNotSerialize] public ValueOutput rebindName;

        protected InputActionRebindingExtensions.RebindingOperation rebindOperation;
        protected string rebindNameResult;

        protected override void Definition()
        {
            base.Definition();
            enableActionOnRebind = ValueInput(nameof(enableActionOnRebind), true);
            disableActionBeforeRebind = ValueInput(nameof(disableActionBeforeRebind), true);
            onRebind = ControlOutput(nameof(onRebind));
            rebindName = ValueOutput(nameof(rebindName), (flow) => { return rebindNameResult; });
        }

        protected override void OnActionStored(Flow flow, InputAction inputAction)
        {
            var dis = flow.GetValue<bool>(disableActionBeforeRebind);
            if (dis)
                inputAction.Disable();

            rebindOperation = inputAction.PerformInteractiveRebinding()
                .WithControlsExcluding("<Mouse>/position")
                .WithControlsExcluding("<Mouse>/delta")
                .WithControlsExcluding("<Gamepad>/Start")
                .WithControlsExcluding("<Keyboard>/p")
                .WithControlsExcluding("<Keyboard>/escape")
                .OnMatchWaitForAnother(0.1f)
                .OnComplete(operation => 
                {
                    int controlBindingIndex = inputAction.GetBindingIndexForControl(inputAction.controls[0]);
                    rebindNameResult = InputControlPath.ToHumanReadableString(inputAction.bindings[controlBindingIndex].effectivePath, InputControlPath.HumanReadableStringOptions.OmitDevice);
                    flow.Invoke(onRebind);

                    var en = flow.GetValue<bool>(enableActionOnRebind);
                    if (en)
                        inputAction.Enable();

                    rebindOperation.Dispose();
                });

            rebindOperation.Start();
        }
    }
}


