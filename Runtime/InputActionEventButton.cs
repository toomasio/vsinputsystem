﻿using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Digitom.VSInputSystem.RunTime
{
    public class InputActionEventButton : InputSystemActionEventBase<bool>
    {
        protected override bool ReadOutputValue(InputAction action)
        {
            return action.phase == InputActionPhase.Started;
        }
    }
}


