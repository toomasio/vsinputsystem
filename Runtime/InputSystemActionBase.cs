﻿using Unity.VisualScripting;
using UnityEngine.InputSystem;
using System;

namespace Digitom.VSInputSystem.RunTime
{
    [UnitCategory("InputSystem/InputAction/Units")]
    public abstract class InputSystemActionBase : Unit
    {
        [DoNotSerialize, PortLabelHidden] public ValueInput action;

        protected override void Definition()
        {
            action = ValueInput(nameof(action), (InputAction)default);
        }

    }
}


