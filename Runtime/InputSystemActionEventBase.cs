﻿using UnityEngine.InputSystem;
using System;
using Unity.VisualScripting;
using UnityEngine;

namespace Digitom.VSInputSystem.RunTime
{
    [UnitCategory("InputSystem/InputAction/Events")]
    public abstract class InputSystemActionEventBase<T> : MachineEventUnit<EmptyEventArgs>
        where T : struct
    {
        protected override string hookName =>
            InputSystem.settings.updateMode ==
            InputSettings.UpdateMode.ProcessEventsInDynamicUpdate
            ? EventHooks.Update
            : EventHooks.FixedUpdate;

        public enum TriggerType { OnPressed, OnHold, OnRelease }
        [Serialize, Inspectable, UnitHeaderInspectable] public TriggerType triggerType;
        [DoNotSerialize, PortLabelHidden] public ValueInput inputAction;
        [PortLabelHidden] public ValueOutput outputValue;

        protected InputAction action;
        protected bool actionRunning;
        protected T _outputValue;

        protected override void Definition()
        {
            base.Definition();
            inputAction = ValueInput(nameof(inputAction), (InputAction)default);
            outputValue = ValueOutput(nameof(outputValue), _ => _outputValue);
            
        }

        public override void StartListening(GraphStack stack)
        {
            base.StartListening(stack);
            var graphReference = stack.ToReference();

            action = Flow.FetchValue<InputAction>(inputAction, graphReference);
        }

        public override void StopListening(GraphStack stack)
        {
            base.StopListening(stack);
            action = null;
        }

        protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args)
        {
            if (action == null) return false;

            var triggered = triggerType == TriggerType.OnPressed ? action.triggered :
                triggerType == TriggerType.OnHold ? action.phase == InputActionPhase.Started :
                action.phase != InputActionPhase.Started && actionRunning;

            actionRunning = action.phase == InputActionPhase.Started;

            _outputValue = ReadOutputValue(action); 
            flow.SetValue(outputValue, _outputValue);

            return triggered;
        }

        protected abstract T ReadOutputValue(InputAction action);
    }
}


