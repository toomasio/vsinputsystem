﻿using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Digitom.VSInputSystem.RunTime
{
    public class InputActionEventFloat : InputSystemActionEventBase<float>
    {
        protected override float ReadOutputValue(InputAction action)
        {
            return action.ReadValue<float>();
        }
    }
}


