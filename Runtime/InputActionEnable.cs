﻿using UnityEngine.InputSystem;
using Unity.VisualScripting;
using System;

namespace Digitom.VSInputSystem.RunTime
{
    public class InputActionEnable : InputSystemActionFlowBase
    {
        [DoNotSerialize, PortLabelHidden] public ValueInput enable;

        protected override void Definition()
        {
            base.Definition();
            enable = ValueInput(nameof(enable), (bool)default);
        }

        protected override void OnActionStored(Flow flow, InputAction inputAction)
        {
            var en = flow.GetValue<bool>(enable);
            if (inputAction != null)
            {
                if (en)
                    inputAction.Enable();
                else
                    inputAction.Disable();
            }
        }
    }
}


