﻿using UnityEngine.InputSystem;
using Unity.VisualScripting;
using System;

namespace Digitom.VSInputSystem.RunTime
{
    public class InputActionMapEnable : InputSystemMapFlowBase
    {
        [DoNotSerialize, PortLabelHidden] public ValueInput enable;

        protected override void Definition()
        {
            base.Definition();
            enable = ValueInput(nameof(enable), (bool)default);
        }

        protected override void OnActionMapStored(Flow flow, InputActionMap inputActionMap)
        {
            var en = flow.GetValue<bool>(enable);
            if (inputActionMap != null)
            {
                if (en)
                    inputActionMap.Enable();
                else
                    inputActionMap.Disable();
            }
        }
    }
}


