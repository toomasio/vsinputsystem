﻿using Unity.VisualScripting;
using UnityEngine.InputSystem;
using System;

namespace Digitom.VSInputSystem.RunTime
{
    public class InputActionReferenceGetMapID : InputSystemActionReferenceBase
    {
        [DoNotSerialize, PortLabelHidden] public ValueOutput id;

        protected override void Definition()
        {
            base.Definition();
            id = ValueOutput(nameof(id), (flow) => 
            {
                var actRef = flow.GetValue<InputActionReference>(actionReference);
                return actRef != null ? actRef.action.actionMap.id : default;
            });
        }

    }
}


