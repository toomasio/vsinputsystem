﻿using Unity.VisualScripting;
using UnityEngine.InputSystem;
using System;

namespace Digitom.VSInputSystem.RunTime
{
    public abstract class InputSystemMapFlowBase : InputSystemMapBase
    {
        [DoNotSerialize, PortLabelHidden] public ControlInput enter;
        [DoNotSerialize, PortLabelHidden] public ControlOutput exit;

        protected override void Definition()
        {
            base.Definition();
            enter = ControlInput(nameof(enter), (flow) => 
            {
                var map = flow.GetValue<InputActionMap>(this.map);
                OnActionMapStored(flow, map);
                return exit;
            });
            exit = ControlOutput(nameof(exit));
        }

        protected abstract void OnActionMapStored(Flow flow, InputActionMap inputActionMap);
    }
}


