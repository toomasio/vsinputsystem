﻿using Unity.VisualScripting;
using UnityEngine.InputSystem;
using System;

namespace Digitom.VSInputSystem.RunTime
{
    [UnitCategory("InputSystem/InputActionMap/Units")]
    public abstract class InputSystemMapBase : Unit
    {
        [DoNotSerialize, PortLabelHidden] public ValueInput map;

        protected override void Definition()
        {
            map = ValueInput(nameof(map), (InputActionMap)default);
        }

    }
}


