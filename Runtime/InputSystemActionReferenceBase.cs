﻿using Unity.VisualScripting;
using UnityEngine.InputSystem;
using System;

namespace Digitom.VSInputSystem.RunTime
{
    [UnitCategory("InputSystem/ActionReference")]
    public abstract class InputSystemActionReferenceBase : Unit
    {
        [DoNotSerialize, PortLabelHidden] public ValueInput actionReference;

        protected override void Definition()
        {
            actionReference = ValueInput(nameof(actionReference), (InputActionReference)default);
        }

    }
}


