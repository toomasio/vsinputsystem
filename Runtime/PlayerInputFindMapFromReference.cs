﻿using Unity.VisualScripting;
using UnityEngine.InputSystem;
using System;
using UnityEngine;

namespace Digitom.VSInputSystem.RunTime
{
    public class PlayerInputFindMapFromReference : InputSystemPlayerInputBase
    {
        [DoNotSerialize, PortLabelHidden] public ValueInput actionReference;
        [DoNotSerialize, PortLabelHidden] public ValueOutput map;

        protected override void Definition()
        {
            base.Definition();
            actionReference = ValueInput(nameof(actionReference), (InputActionReference)default);

            map = ValueOutput(nameof(map), (flow) => 
            {
                var inp = flow.GetValue<PlayerInput>(playerInput);
                var actRef = flow.GetValue<InputActionReference>(actionReference);
                return inp && actRef != null ? inp.actions.FindActionMap(actRef.action.actionMap.id) : null;
            });
        }

    }
}


