﻿using Unity.VisualScripting;
using UnityEngine.InputSystem;
using System;

namespace Digitom.VSInputSystem.RunTime
{
    [UnitCategory("InputSystem/PlayerInput")]
    public abstract class InputSystemPlayerInputBase : Unit
    {
        [DoNotSerialize, NullMeansSelf, PortLabelHidden] public ValueInput playerInput;

        protected override void Definition()
        {
            playerInput = ValueInput(nameof(playerInput), (PlayerInput)default);
            playerInput.NullMeansSelf();
        }

    }
}


