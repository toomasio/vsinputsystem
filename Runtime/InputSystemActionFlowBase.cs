﻿using Unity.VisualScripting;
using UnityEngine.InputSystem;
using System;

namespace Digitom.VSInputSystem.RunTime
{
    [UnitCategory("InputSystem/InputAction/Units")]
    public abstract class InputSystemActionFlowBase : InputSystemActionBase
    {
        [DoNotSerialize, PortLabelHidden] public ControlInput enter;
        [DoNotSerialize, PortLabelHidden] public ControlOutput exit;

        protected override void Definition()
        {
            base.Definition();
            enter = ControlInput(nameof(enter), (flow) => 
            {
                var act = flow.GetValue<InputAction>(action);
                OnActionStored(flow, act);
                return exit;
            });
            exit = ControlOutput(nameof(exit));
        }

        protected abstract void OnActionStored(Flow flow, InputAction inputAction);
    }
}


