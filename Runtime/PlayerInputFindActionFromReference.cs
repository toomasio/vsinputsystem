﻿using Unity.VisualScripting;
using UnityEngine.InputSystem;
using System;
using UnityEngine;

namespace Digitom.VSInputSystem.RunTime
{
    public class PlayerInputFindActionFromReference : InputSystemPlayerInputBase
    {
        [DoNotSerialize, PortLabelHidden] public ValueInput actionReference;
        [DoNotSerialize, PortLabelHidden] public ValueOutput action;

        protected override void Definition()
        {
            base.Definition();
            actionReference = ValueInput(nameof(actionReference), (InputActionReference)default);

            action = ValueOutput(nameof(action), (flow) => 
            {
                var inp = flow.GetValue<PlayerInput>(playerInput);
                var actRef = flow.GetValue<InputActionReference>(actionReference);
                return inp && actRef != null ? inp.actions.FindAction(actRef.action.id) : null;
            });
        }

    }
}


