﻿using UnityEngine.InputSystem;
using System;
using Unity.VisualScripting;

namespace Digitom.VSInputSystem.RunTime
{
    public class InputActionData<T> : EventUnit<EmptyEventArgs>.Data
    {
        public T inputValue;
        public InputAction action;
        public Action<InputAction.CallbackContext> startedHandler;
        public Action<InputAction.CallbackContext> performedHandler;
        public Action<InputAction.CallbackContext> canceledHandler;
    }

    [UnitCategory("Events/InputSystem")]
    public abstract class InputSystemEvent<T> : MachineEventUnit<EmptyEventArgs>
    {
        public enum TriggerType { OnStarted, OnPerformed, OnCanceled }

        public override IGraphElementData CreateData()
        {
            return new InputActionData<T>();
        }

        [NullMeansSelf] [DoNotSerialize] public ValueInput playerInput;
        [DoNotSerialize] public ValueInput action;
        [DoNotSerialize] public ValueInput triggerType;
        [DoNotSerialize] public ValueOutput value;

        protected override string hookName => EventHooks.Custom;

        protected abstract T GetInputData(InputActionData<T> data, InputAction.CallbackContext ctx);

        protected override void Definition()
        {
            base.Definition();

            playerInput = ValueInput("playerInput", (PlayerInput)null).NullMeansSelf();
            action = ValueInput(nameof(action), (InputActionReference)null);
            triggerType = ValueInput(nameof(triggerType), TriggerType.OnPerformed);

            value = ValueOutput("value", (flow) =>
            {
                var data = flow.stack.GetElementData<InputActionData<T>>(this);
                return data.inputValue;
            });
        }

        public override void StartListening(GraphStack stack)
        {
            base.StartListening(stack);
            var data = stack.GetElementData<InputActionData<T>>(this);
            var flow = Flow.New(stack.ToReference());
            var trig = flow.GetValue<TriggerType>(triggerType);
            var input = flow.GetValue<PlayerInput>(playerInput);
            var actionRef = flow.GetValue<InputActionReference>(action);

            if (input != null && actionRef != null)
            {
                var actionValue = input.actions.FindAction(actionRef.action.id);
                if (actionValue != null)
                {
                    data.action = actionValue;
                    data.startedHandler = (ctx) =>
                    {
                        if (trig != TriggerType.OnStarted) return;
                        data.inputValue = GetInputData(data, ctx);
                        flow.Invoke(trigger);
                    };
                    data.performedHandler = (ctx) =>
                    {
                        if (trig != TriggerType.OnPerformed) return;
                        data.inputValue = GetInputData(data, ctx);
                        flow.Invoke(trigger);
                    };
                    data.canceledHandler = (ctx) =>
                    {
                        if (trig != TriggerType.OnCanceled) return;
                        data.inputValue = GetInputData(data, ctx);
                        flow.Invoke(trigger);
                    };
                    actionValue.started += data.startedHandler;
                    actionValue.performed += data.performedHandler;
                    actionValue.canceled += data.canceledHandler;
                }
            }
        }

        public override void StopListening(GraphStack stack)
        {
            base.StopListening(stack);
            var data = stack.GetElementData<InputActionData<T>>(this);
            if (data.action == null) return;
            data.action.started -= data.startedHandler;
            data.action.performed -= data.performedHandler;
            data.action.canceled -= data.canceledHandler;
        }
    }
}


