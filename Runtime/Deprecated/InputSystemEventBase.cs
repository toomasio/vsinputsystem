﻿using UnityEngine.InputSystem;
using System;
using Unity.VisualScripting;

namespace Digitom.VSInputSystem.RunTime
{
    public class InputSystemEventBaseData : EventUnit<EmptyEventArgs>.Data
    {
        public PlayerInput input;
    }

    [UnitCategory("InputSystem/Events")]
    public abstract class InputSystemEventBase<T> : MachineEventUnit<EmptyEventArgs>
        where T : InputSystemEventBaseData, new()
    {
        public override IGraphElementData CreateData()
        {
            return new T();
        }

        [NullMeansSelf] [DoNotSerialize] public ValueInput playerInput;

        protected override string hookName => EventHooks.Custom;

        protected override void Definition()
        {
            base.Definition();

            playerInput = ValueInput("playerInput", (PlayerInput)null).NullMeansSelf();
        }

        public override void StartListening(GraphStack stack)
        {
            base.StartListening(stack);
            var data = GetElementData(stack);
            var flow = Flow.New(stack.ToReference());
            var input = flow.GetValue<PlayerInput>(playerInput);

            if (input != null)
            {
                data.input = input;
                OnInputFound(data, flow);
            }
        }

        protected virtual T GetElementData(GraphStack stack)
        {
            return stack.GetElementData<T>(this);
        }
        protected abstract void OnInputFound(T data, Flow flow);
    }
}


