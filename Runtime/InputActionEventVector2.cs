﻿using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Digitom.VSInputSystem.RunTime
{
    public class InputActionEventVector2 : InputSystemActionEventBase<Vector2>
    {
        protected override Vector2 ReadOutputValue(InputAction action)
        {
            return action.ReadValue<Vector2>();
        }
    }
}


