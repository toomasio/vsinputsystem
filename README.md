Install Instructions:
----------------------
Unity -> Window -> Package Manager -> "+" -> Add package from git URL ... -> https://gitlab.com/toomasio/vsinputsystem.git

Update instructions:
---------------------
Delete the 'packages-lock.json' file in your YourUnityProject\Packages

Nodes included in this package:
---------------------------------
- Input System Button
- Input System Axis
- Input System Vector2
